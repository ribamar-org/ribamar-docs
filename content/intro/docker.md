---
title: Start With Docker
weight: 1
---

Check the following list:
- Pull the container with `docker pull ribamarorg/ribamar`.
- There must be a running MongoDB instance (possibly in a container too).
- Ribamar container must be able to reach the database.
- Ribamar container will read a [configuration file](https://ribamar-org.gitlab.io/ribamar/settings/) if mounted in `/usr/src/ribamar/conf.yml`.

The following files may help you to `docker-compose up -d ribamar`:

```yml
# conf.yml
database:
  url: mongodb://database:27017
```

```yml
# docker-compose.yml
version: '3.7'
services:
  app:
    image: ribamarorg/ribamar
    ports:
      - "6776:6776"
    volumes:
      - "./conf.yml:/usr/src/ribamar/conf.yml"
    depends_on:
      - database
  database:
    image: mongo
```
