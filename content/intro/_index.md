---
title: Quick Start
weight: 1
chapter: true
---

# Quick Start

1. Ribamar requires the installation of [NodeJS](https://nodejs.org/en/download/current/) and [MongoDB Community Server](https://www.mongodb.com/download-center#community) to properly work.
2. Once Node is up, install Ribamar globally with: `npm i -g ribamar`.
3. Once MongoDB is up, start Ribamar Server with: `ribamar <optional path to config>`.
4. In your browser, hit `http://localhost:6776`.
5. You have successfuly started quickly!

> Or [start with Docker](docker).
