---
title: Ribamar Docs
chapter: true
---

<img width="280" src="images/text-logo-white.png" alt="Ribamar" />

Ribamar is an open-source RESTful microservice for managing user accounts, authentication and authorization. Send confirmation e-mails; manage groups; store general user data and much more.

## Index
- [Quick Start](./intro)
- [Start with Docker](./intro/docker)
- [REST API Reference](./api)
- [Contributing](./contribution)
