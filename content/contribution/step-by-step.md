---
title: Step by Step
weight: 1
---

1. Install the [recommended development toolchain](#toolchain).
2. [Fork ribamar in gitlab](https://gitlab.com/riba-riba/ribamar/forks/new).
3. Create a branch in the forked repo for your code.
4. Code your most awesome ideas.
5. Ensure your code follow our [cool code styling](#code-style).
6. [Add full test coverage to your code and debug it](#testing).
7. Ensure it passes on all pipelines.
8. [Submit a Merge Request](https://gitlab.com/riba-riba/ribamar/merge_requests/new) of your branch targeting our `master` or the related protected branch.
9. [Be cool!](#community-sense)
10. In the Merge Request, help us to review and fine tune your contribution.

If all goes well, your fingerprints will soon be all over Ribamar.

{{% notice note %}}
We may eventualy find the need to edit your commits' order or messages, but we will always keep your name on it.
{{% /notice %}}
