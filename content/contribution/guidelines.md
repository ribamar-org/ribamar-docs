---
title: Guidelines
weight: 4
---

Below we present you our cool guideline for how to make **delightful contribution**. Be sure to adhere so we all can embrace your work as quickly as possible.

### Code Style
1. Comment *all* section of code. Group your code in little blocks and tag them with brief single-line comments.

```js

// Do some cool stuff
code code code...
code code code...
code code code...
```
2. Implement all suggestions (warnings) ESLint gives you. Our main code style is setup in `.eslintrc.yml`.
3. **Bonus Tip:** Be sure to check some files already in the poroject to get a grasp of our style.

### Community Sense
1. Be respectful and polite in all communication channels.
2. Consider all suggestions and ideas other may present you the same way you expect others to consider your contributions.
3. Be open to constructive criticism.
