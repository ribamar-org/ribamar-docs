---
title: Testing
weight: 3
---

We use [Mocha](https://mochajs.org/) for unit-testing our JS, and [NYC](https://istanbul.js.org/) for test coverage reports.

All code in Ribamar must be unit-tested from the start, so try your best to include you work in the `test/spec.js` file. Our team will also be at your aid in MRs to help you with testing.

Be sure to check other modules' test cases to inspire your own.

In order to find bugs and test your code, you must simply `cd` to the project's directory nd run: `nyc npm t`. This command will run the unit tests and output the coverage report.
