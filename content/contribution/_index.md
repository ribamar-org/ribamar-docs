---
title: Contribution Guide
chapter: true
weight: 4
---

# Contributing
Help us make of Ribamar a tool for you.
We would appreciate very much if you all join your heads to ours.

If you have spot any enhancements to be made and is willing to dirt your hands about it, fork us and [submit your merge request](https://gitlab.com/gitlab-org/ribamar/merge_requests/new) so we can collaborate effectively.

If you are somewhat unfamiliar with the common community workflow, you may head to our [step-by-step contributing guide](step-by-step).
