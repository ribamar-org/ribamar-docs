---
title: Toolchain
weight: 2
---

The recommended tools for contributing to Ribamar are as follow:

- [Atom Text Editor](https://atom.io/) and the following community packages:
  - file-icons
  - linter
  - linter-eslint
  - minimap
  - selection-highlight
- [MongoDB Community Server](https://www.mongodb.com/download-center#community)
- [SmartGit](https://www.syntevo.com/smartgit/download/)
- [NodeJS Latest Version](https://nodejs.org/en/download/current/) and the following global packages:
  - mocha
  - nyc
