---
title: Accounts
weight: 1
---

Accounts are the cornerstone for Ribamar data. It will contain at least one [credential](credential) for [authentication](/authentication), as well as any data you choose to attach to accounts.

## Create Account
> `POST /account`

When successful, this request will register a new account with the specified credential and attached data; and respond JSON with a field `account` containing the generated id for the account (Eg.: "87fa783b765d").

The request body must be JSON as follow:

- `key`: A string to be the unique id for the account's first credential.
- `passcode`: A string to be encrypted for further authentication with the given key.
-  **Any other** field will be stored as is, allowing arbitrary data to be attached to the account.

## Check Account
> `GET /account/:id`

Respond JSON with all data from the account specified by `:id`.

## Update Account Data
> `PATCH /account/:id`

When successful, this request will update the **arbitrary data** of the account identified by `:id`, according to the specifis in request body.

The request body must be JSON as follow:

-  **Any field** wil be stored as is. Existing fields will be overwritten.

## Remove Account
> `DELETE /account/:id`

When successful, this request will remove from Ribamar all data of the account identified by `:id`.

## Search Accounts
> `GET /search ? fields=:field1,field2,...`

Respond with a list of all accounts registered in Ribamar, according to input filters and fields.
