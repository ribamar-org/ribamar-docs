---
title: Miscellaneous
weight: 10
---

Here will be grouped API entities with varies utilities to support general Ribamar managing.

## Notify Account
> `POST /notification`

When successful, this request will send an e-mail notification to the given [Account Address](TODO ).

The request body must be JSON as follow:

- `key`: A string to identify the account to which the notification must be sent.
- `template`: A string to be the name of a setup [e-mail template](TODO ) to be used when rendering the mail body.
- `data` (optional): A object to be used as [bind context](TODO ) when rendering the e-mail body.
