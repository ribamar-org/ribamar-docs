---
title: API Reference
chapter: true
weight: 2
---

# API Reference

Ribamar Server exposes a REST API through which other applications must interact in order to leverage its functionality. In the following pages you will find specifics on each Ribamar API endpoint and its parameters etc...

- [Accounts](./accounts)
- [Credentials and Authentication](./credentials)
- [Miscellaneous](./misc)

## Learn More About
- [REST APIs](https://en.wikipedia.org/wiki/Representational_state_transfer)
