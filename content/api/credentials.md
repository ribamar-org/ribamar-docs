---
title: Credentials and Authentication
weight: 2
---

Credentials are devices to authenticate an external consumer as owner of a given account. One account can have one to many credentials. Credentials allow securing resources based on personal identification. All security data in credentials is encrypted and private to Ribamar, meaning that no consumers ever receive any security tokens.

{{% notice info %}}
All passcodes in Ribamar are PBKDF2 encrypted.
{{% /notice %}}

## Register Credential
> `POST /credential`

When successful, this request will add a new credential to the account specified in the request body.

The request body must be JSON as follow:

- `key`: A string to be the unique id for the credential.
- `passcode`: A string to be encrypted for further authentication with the given key.
- `account`: A string identifying (`id`) the account to which the credential bust be added.

## Change Passcode
> `PATCH /credential/:key`

When successful, this request will change the stored password for the credential identified by `:key`.

The request body must be JSON as follow:

- `passcode`: A string to be encrypted for further authentication with the credential key.

## Alter Credential
> `PUT /credential/:key`

When successful, this request will replace the key and passcode for the credential identified by `:key`.

The request body must be JSON as follow:

- `key`: A string to be the new unique id for the credential.
- `passcode`: A string to be encrypted for further authentication with the credential key.

## Remove Credential
> `DELETE /credential/:key`

When successful, this request will remove from its account, the credential identified by `:key`. This shall fail if the given credential is the only one in its account, in which case you can only remove the entire account.

## Authenticate
> `GET /authentication ? key=:key & passcode=:passcode`

Respond JSON with field `result` containing:

- `"failure"` if passcode doesn't match what is stored, or credential with key doesn't exist.
- `"success"` if passcode matches the one stored for the given key.
