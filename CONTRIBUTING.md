# Contributing to Docs

If you found any problems with documentation or found undocumented functionality or processes, please:
- open an issue to report, or
- open a merge request to fix it

For issues, also mention the label which is closest to the problem you found:
- `~typo`
- `~out of date`
- `~incorrect`
- `~too complex

Thank you for your contribution!
