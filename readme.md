
<img width="320" alt="Ribamar" src="https://i.imgur.com/MNzMeoO.png" />

Ribamar is an open-source back-end application for managing user accounts, authentication and authorization. Send confirmation e-mails; manage groups; store general user data and much more, all through a simple RESTful API.

Ribamar is ideal to work as a micro-service. Just setup, plug and BAM!

## Quick Start
1. Ribamar requires the installation of [NodeJS](https://nodejs.org/en/download/current/) and [MongoDB Community Server](https://www.mongodb.com/download-center#community) to properly work.
2. Once Node is up, install Ribamar globally with: `npm i -g ribamar`.
3. Once MongoDB is up, start Ribamar Server with: `ribamar`.
4. In your browser, hit `http://localhost:6776`.
5. You have successfuly started quickly!

## Reporting Bugs
If you have found any problems in Ribamar be welcome to:

1. [Open an issue](https://gitlab.com/ribamar-org/ribamar/issues/new).
2. Describe what happened and how.
3. Still in the issue text, reference the label `~bug`.

We will make sure to take a look when time allow us.

## Proposing Features
If you wish to have that awesome feature or have any advice for us, be welcome to:
1. [Open an issue](https://gitlab.com/ribamar-org/ribamar/issues/new).
2. Describe your ideas.
3. Still in the issue text, reference the label `~proposal`.

## Contributing
If you have spot any enhancements to be made and is willing to dirt your hands about it, fork us and [submit your merge request](https://gitlab.com/ribamar-org/ribamar/merge_requests/new) so we can collaborate effectively.

Be always sure to follow our cool [guidelines for delightful contribution](wikis/contributing#guidelines).

If you are somewhat unfamiliar with the common community workflow, you may head to our [step-by-step contributing guide](wikis/contributing#setp-by-step).

## Supporting
We have no formal way to receive any donation or alike, so if you happen to feel caring about us, we would be delighted to hear about you directly in [our inboxes](#contact-us).

## Contact Us
- gcsboss@gmail.com
